#include "main.h"

/**
 * main.c
 */
void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    config_pwm_gpio();
    enable_Interrupts();

    //map_ports();

    config_pwm_timer();

    start_pwm(50);

    //TIMER_A0->CTL |= TIMER_A_CTL_IFG;

    while(1);
}

void TA0_N_IRQHandler()
{
    if(TIMER_A0->CTL & BIT0)
        P9->OUT ^= BIT0; // Toggle the pin

    TIMER_A0->CTL &= ~TIMER_A_CTL_IFG;          // Clear the IFG
    TIMER_A0->CCTL[1] &= ~TIMER_A_CCTLN_CCIFG;  // Clear the IFG that set the other IFG
}

void enable_Interrupts()
{
    NVIC->ISER[0] = 1 << ((TA0_N_IRQn) & 31); // Enable interrupt for Port1.
    PCM->CTL1 = PCM_CTL0_KEY_VAL | PCM_CTL1_FORCE_LPM_ENTRY;

    __enable_irq();         // Overall IRQ functionality enable
    __low_power_mode_off_on_exit();

    __DSB();                // Enable Data Synchronization Barrier (protects
                            // against dropped operations during interrupts)
}
