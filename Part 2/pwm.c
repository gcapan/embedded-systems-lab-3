/*
 * pwm.c
 *
 *  Created on: Sep 25, 2019
 *      Author: Grant Capan
 */
#include "pwm.h"

void config_pwm_timer(void)
{
    TIMER_A0->CTL |= TIMER_A_CTL_MC_0;          // StopMode
    TIMER_A0->R = 0b0;                          // Reset timer counter
    TIMER_A0->CTL |= TIMER_A_CTL_TASSEL_2;      // Select the 3MHz Oscillator as source
    TIMER_A0->CTL |= TIMER_A_CTL_ID__8;         // Divide 3MHz by 8
    TIMER_A0->CTL |= TIMER_A_EX0_TAIDEX_4;      // Divide again by 4
    TIMER_A0->CTL |= TIMER_A_CTL_IE;            // Enable interrupts.
    //TIMER_A0->CCTL[1] |= TIMER_A_CCTLN_OUTMOD_7;
    //TIMER_A0->CCR[0] |= 0b00000000;
}

void start_pwm(uint8_t duty_cycle)
{
    //TIMER_A0->R = 0b0;                          // Reset timer counter

    //TIMER_A0->CCTL[0] |= TIMER_A_CCTLN_CCIE;
    TIMER_A0->CCTL[1] |= TIMER_A_CCTLN_CCIE;
    TIMER_A0->CCTL[1] |= TIMER_A_CCTLN_OUTMOD_4;
    //TIMER_A0->CCTL[1] |= TIMER_A_CCTLN_OUTMOD_7;
    //TIMER_A0->CCR[0] |=  0b00011110;
    TIMER_A0->CCR[0] |= REDUCED_SCLK / DESIRED_PERIOD;
    //TIMER_A0->CCR[0] |= REDUCED_SCLK / DESIRED_PERIOD;  // Set the total number of ticks per cycle based on the
                                                        // values defined in pwm.h
    //TIMER_A0->CCR[1] |=  (int)((float)TIMER_A0->CCR[0] * ((float)duty_cycle / 100));    // Calculate the number of ticks needed
                                                                                        // to achieve the specified duty cycle.
    TIMER_A0->CCR[1] |= TIMER_A0->CCR[0] - 1;
    TIMER_A0->CTL |= TIMER_A_CTL_MC__UP;        // Up Mode

}

void stop_pwm(void)
{
    TIMER_A0->CTL &= ~TIMER_A_CTL_MC__UP;
    TIMER_A0->CTL |= TIMER_A_CTL_MC_1;          // StopMode
}

void config_pwm_gpio(void)
{
    P1->DIR = 0b11111111;       // Config all ports as output
    P1->OUT &= ~BIT0;
    P1->SEL0 = 0;
    P1->SEL1 = 0;
    //P1->SEL0 = 0b00000000;               // Set port 1 pin 0 mode to pmap
    //P1->SEL1 = 0b00000000;

    P9->DIR = 0b11111111;
    P9->OUT &= ~BIT0;
    P9->SEL0 = 0;
    P9->SEL1 = 0;

    P2->DIR |= 0b00010111;      // Set pin 2.4 to output

    P2->SEL0 |= 0b10111;
    P2->SEL1 = 0b00000000;

}

void map_ports()
{
    PMAP->KEYID = PMAP_KEYID_VAL;                   // Grants access to all port mapping configuration registers.
    PMAP->CTL = PMAP_CTL_PRECFG;                 // Enable reconfiguration of port mappings.
    P2MAP->PMAP_REGISTER0 = PM_TA0CCR1A;    // Map Port 2, Pin 0 (LED2 Red) to the output of Timer A0 CCR1
    P2MAP->PMAP_REGISTER1 = PM_TA0CCR1A;    // Map Port 2, Pin 1 (LED2 Green) to the output of Timer A0 CCR1
    P2MAP->PMAP_REGISTER2 = PM_TA0CCR1A;    // Map Port 2, Pin 2 (LED2 Blue) to the output of Timer A0 CCR1
    PMAP->KEYID = 0;
}


