#include "main.h"

/**
 * main.c
 */
void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer

	config_pwm_gpio();

	map_ports();

	config_pwm_timer();

	start_pwm(50);

	while(1);
}
